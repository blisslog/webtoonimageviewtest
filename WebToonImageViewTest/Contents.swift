//
//  Contents.swift
//  LezhinTest
//
//  Created by 이정훈 77/11/23 on 2018. 10. 19..
//  Copyright © 2018년 App. All rights reserved.
//

import UIKit

class Meta {
    var current_page = 0
    
    var is_end: Bool = false
    var pageable_count: Int = 0
    var total_count: Int32 = 0
    
    /* 외부 사용하는 api의 오류를 사전에 방지 하고자 마지막페이지 정보에 대한 보완 */
    func isLastPage() -> Bool {
        if is_end {
            return true
        }
        else {
            return current_page >= pageable_count
        }
    }
    
    /*
        호출 실패나 재호출의 이슈로 오류를 방지 하기 위해 호출을 위한 next()와 성공시 페이지 정보 저장을 위한 fix_page_count() 함수를 별도 지정
    */
    func next() -> String {
        return "\(current_page + 1)"
    }
    
    func fix_page_count() {
        current_page += 1
        print("Current Page : \(current_page)")
    }
}

class Content {
    var collection: String = ""
    var datetime: String = ""
    var display_sitename: String = ""
    var doc_url: String = ""
    var width: CGFloat = 0.0
    var height: CGFloat = 0.0
    var image_url: String = ""
    var thumbnail_url: String = ""
}
