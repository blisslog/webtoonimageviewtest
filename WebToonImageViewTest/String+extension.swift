//
//  String+extension.swift
//  LezhinTest
//
//  Created by 이정훈 77/11/23 on 2018. 10. 22..
//  Copyright © 2018년 App. All rights reserved.
//

import UIKit

public extension String {

    func trimmed() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var length: Int {
        get {
            return self.count
        }
    }
}
