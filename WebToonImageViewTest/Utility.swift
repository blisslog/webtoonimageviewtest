//
//  Utility.swift
//  LezhinTest
//
//  Created by 이정훈 77/11/23 on 2018. 10. 19..
//  Copyright © 2018년 App. All rights reserved.
//

import UIKit

class Utility {

    // MARK: block 내부를 delay 후 실행
    static func runAfterDelay(_ delay: TimeInterval, block: @escaping ()->()) {
        let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute: block)
    }
    
    // MARK: Alert
    static func alert(_ text:String, obj:AnyObject) {
        let alertController = UIAlertController(title: "", message: text, preferredStyle: UIAlertController.Style.alert)
        let cancel = UIAlertAction(title: "확인", style: UIAlertAction.Style.cancel) { (action: UIAlertAction) -> Void in
            obj.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(cancel)
        
        obj.present(alertController, animated: true, completion: nil)
        
    }
    
}
