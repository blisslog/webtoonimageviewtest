//
//  ViewController.swift
//  LezhinTest
//
//  Created by 이정훈 77/11/23 on 2018. 10. 19..
//  Copyright © 2018년 App. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var moreBtn: UIButton!
    
    let kakaoApiLink = "https://dapi.kakao.com/v2/search/image"
    let kakaoAuthKey = "KakaoAK c7809c8b950a0a0af4ce0f06ac203fec"
    
    var contents = [Content]()
    var metaInfo: Meta = Meta()
    var isNoneData: Bool = false
    var ingSearch: Bool = false
    var isMore: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSearch()
        tableView.rowHeight = UITableView.automaticDimension
        
        textfield.becomeFirstResponder()
    }
    
    func requestSearchAPI(_ searchText: String) {
        let keywordWebLink = "\(kakaoApiLink)?query=\(searchText)&page=" + metaInfo.next()
        
        // 인코딩
        let searchLink = keywordWebLink.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // 검색 API 호출
        Alamofire
            .request(searchLink, method: .get, encoding: JSONEncoding.prettyPrinted, headers: ["Authorization": kakaoAuthKey])
            .responseJSON { response in
            
            self.finishSearch()
                
            switch response.result {
            case .success(let result):
                print(result)
                if let JSON = result as? [String:Any] {
                    
                    // Contents Info
                    let dataArr:NSArray = JSON["documents"] as! NSArray
                    
                    for dataDic in (dataArr as? [NSDictionary])! {
                        let content = Content()
                        content.width = (dataDic["width"] as? CGFloat)!
                        content.height = (dataDic["height"] as? CGFloat)!
                        content.image_url = (dataDic["image_url"] as? String)!
                        
                        self.contents.append(content)
                    }
                    
                    if self.contents.count == 0 {
                        self.noneData()
                    }
                    
                    // Meta Info
                    let metaData = JSON["meta"] as! NSDictionary
                    
                    self.metaInfo.is_end = metaData["is_end"] as! Bool
                    self.metaInfo.pageable_count = metaData["pageable_count"] as! Int
                    self.metaInfo.total_count = metaData["total_count"] as! Int32
                    
                    self.metaInfo.fix_page_count()
                    
                    self.tableView.reloadData()
                    
                    if self.contents.count == 0 {
                        self.noneData()
                    }
                    
                    if self.contents.count == 0 || self.metaInfo.isLastPage() || self.metaInfo.is_end {
                        self.moreBtn.alpha = 0.0
                        self.moreBtn.isHidden = true
                    }
                    else {
                        self.moreBtn.alpha = 1.0
                        self.moreBtn.isHidden = false
                    }
                }
                
            case .failure(let error):
                
                self.noneData()
                
                print("Error: \(error)")
                DispatchQueue.main.async {
                    Utility.alert("Error가 발생했습니다.\n다시 검색해주세요.", obj: self)
                }
                
            }
            
        }
        
    }

    // MARK: - UITableViewDataSource, UITableViewdelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if contents.count == 0 {
            return isNoneData ? 1 : 0
        }
        else {
            return contents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isNoneData {
            let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "none_data_cell")!
            
            return cell
        }
        else {
            let cell:ImageViewCell = self.tableView.dequeueReusableCell(withIdentifier: "image_cell")! as! ImageViewCell
            
            let content = contents[indexPath.row]
            let url = URL(string: content.image_url)!
            
            // 이미지 비율에 맞게 가득채우기
            let tableview_width = tableView.contentSize.width
            let width_ratio = tableview_width / content.width
            let height_value = content.height * width_ratio
            cell.imgHeight.constant = height_value
            
            // 이미지 세팅 Kingfisher
            cell.imgView.kf.setImage(with: url, placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: { receivedSize, totalSize in }, completionHandler: { image, error, cacheType, imageURL in })
            
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hideKeyboard()
    }
    
    // MARK: - ScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideKeyboard()
    }
    
    // MARK: - UITextFieldDelegate
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing : \(textField.text!)")
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("textField : \(textField.text!), string: \(string)")
        
        metaInfo.current_page = 0
        
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(self.searchImage), with: nil, afterDelay: 1.0)
        
        return true
    }
    
    // MARK: - IBAction
    @IBAction func pressedMoreBtn(_ sender: Any) {
        if (metaInfo.isLastPage() || metaInfo.is_end) == false {
            isMore = true
            moreSearchImage()
        }
    }
    
    // MARK: - Normal Function
    @objc func searchImage() {
        if ingSearch {
            return
        }
        
        clearData()
        
        let searchText = textfield.text!.trimmed()
        if searchText.length > 0 {
            startSearch()
            hideKeyboard()
            requestSearchAPI(searchText)
        }
    }
    
    func moreSearchImage() {
        if ingSearch {
            return
        }
        if metaInfo.isLastPage() == false {
            startSearch()
            let searchText = textfield.text!.trimmed()
            requestSearchAPI(searchText)
        }
        else {
            isMore = false
        }
    }
    
    func clearData() {
        contents.removeAll()
        isNoneData = false
        tableView.scrollsToTop = true
        tableView.reloadData()
    }
    
    func noneData() {
        isNoneData = true
    }
    
    func initSearch() {
        self.moreBtn.alpha = 0.0
        self.moreBtn.isHidden = true
        
        finishSearch()
    }
    
    func startSearch() {
        ingSearch = true
        if isMore == false {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
        }
    }
    
    func finishSearch() {
        isMore = false
        ingSearch = false
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        
    }
    
    func hideKeyboard() {
        textfield.resignFirstResponder()
    }
}

